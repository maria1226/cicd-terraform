# Simple cicd with [Terraform](https://www.terraform.io)

## Prerequisite:
- Base knowledge about [Gitlab CI](https://docs.gitlab.com/ee/ci/)
- Base knowledge about [Terraform](https://www.terraform.io)
  - [null_resource](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource)

### Stages:
  - tf_validate - This will validates the configuration files in a directory ---> [terraform validate](https://www.terraform.io/docs/cli/commands/validate.html)
  - tf_plan - This will creates an execution plan ---> [terraform plan](https://www.terraform.io/docs/cli/commands/plan.html)
  - tf_apply - This will apply the generated plan ---> [terrafrom apply](https://www.terraform.io/docs/cli/commands/apply.html)

### Lets Start...
<img src="https://octodex.github.com/images/gobbleotron.gif" alt="alt text" width="200"/>




